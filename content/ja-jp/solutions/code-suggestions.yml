title: GitLabのAI支援によるコードの提案
og_title: GitLabのAI支援によるコードの提案
description: GitLabのAI支援によるコードの提案。独自のソースコードを安全に保ちます。コーディングの生産性を向上できます。何十億行ものコードをすぐに使用できます。
twitter_description: GitLabのAI支援によるコードの提案。独自のソースコードを安全に保ちます。コーディングの生産性を向上できます。何十億行ものコードをすぐに使用できます。
og_description: GitLabのAI支援によるコードの提案。独自のソースコードを安全に保ちます。コーディングの生産性を向上できます。何十億行ものコードをすぐに使用できます。
og_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
twitter_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
hero: 
  note: GitLabのAI支援によるコードの提案
  description: 
    - typed:
        - フローを継続できるよう
      is_description_inline: true
      highlighted: お手伝い
    - typed:
        - お気に入りの
      highlighted: IDEで提供
      is_description_inline: true
    - typed:
        - お使いの
      highlighted: 言語で
      is_description_inline: true
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: AIロゴ
  button:
    text: コードの提案を無料でお試しください
    href: /solutions/code-suggestions/sales
    variant: secondary
copyBlocks:
  blocks: 
    - title: コーディングを高速化
      description: コードの提案は、前後関係に沿ったコードブロックの予測および補完、関数宣言のロジックの定義および生成、テストの生成、正規表現のような一般的なコードの提案を行うことで、開発者がフローを継続できるように支援します。そのため、チームがより速く、より効率的にソフトウェアを作成できるようになります。
    - title: 専有コードはトレーニングデータとして使用しない
      description: コードの提案は、プライバシーを重視して構築されています。GitLabに保存されているお客様の非公開のコードを、トレーニングデータとして使用することはありません。コードの提案を使用する際の[データの使用方法についてはこちら](https://docs.gitlab.com/ee/user/ai_data_usage.html){data-ga-name="code suggestions data usage" data-ga-location="body"}をご覧ください。
      img:
        src: /nuxt-images/solutions/code-suggestions/secure-code.svg
        alt: 安全なコードのロゴ

    - title: お使いの言語でサポート
      description: 'AIを活用して、あなたの作業方法に合ったコードを提案します。C++、C#、Go、Google SQL、Java、JavaScript、Kotlin、PHP、Python、Ruby、Rust、Scala、Swift、TypeScriptの[14言語](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-languages){data-ga-name="code suggestions supported languages" data-ga-location="body"}でご利用いただけます。'
      logos: [
        "/nuxt-images/logos/c-logo.svg",
        "/nuxt-images/logos/cpp-logo.svg",
        "/nuxt-images/logos/c-sharp-logo.svg",
        "/nuxt-images/logos/javascript-logo.svg",
        "/nuxt-images/logos/typescript-logo.svg",
        "/nuxt-images/logos/python-logo.svg",
        "/nuxt-images/logos/java-logo.svg",
        "/nuxt-images/logos/ruby-logo.svg",
        "/nuxt-images/logos/rust-logo.svg",
        "/nuxt-images/logos/scala-logo.svg",
        "/nuxt-images/logos/kotlin-logo.svg",
        "/nuxt-images/logos/go-logo.svg",
        "/nuxt-images/logos/php-logo.svg",
      ]
    - title: お気に入りのIDEで提供
      description: 'GitLabの拡張機能は、人気のIDEのマーケットプレイスから入手できます。GitLab Web IDE、VS Code、Visual Studio、JetbrainsベースIDE、NeoVIMなどをサポートしています。コードの提案で[サポートされるIDE](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html){data-ga-name="code suggestions IDE support" data-ga-location="body"}の詳細についてご確認ください。'
      languageSelected: 'go'
    - title: Self-Managedインスタンスで利用可能
      description: Self-Managedインスタンスでも、GitLab.comへの安全な接続を介して、コードの提案をご利用いただけるようになりました。[詳細をご覧ください](https://docs.gitlab.com/ee/user/ai_features_enable.html#configure-gitlab-duo-on-a-self-managed-instance){data-ga-name="code suggestions self managed" data-ga-location="body"}。
      languageSelected: 'javascript'
ctaBlock:
    title: AIペアプログラマーのための今後の展開は？
    cards:
      - header: 提案の品質向上
        description: |
          GitLabでは、新しいプロンプトエンジニアリング、インテリジェントなルーティングモデル、また推論画面でのコンテキストの拡張によって、提案の品質を継続的に改善しています。[こちらから進捗状況を確認したり、アイデアを提案していただけます](https://gitlab.com/groups/gitlab-org/-/epics/9814)。
        icon: ai-code-suggestions
      - header: 一般提供
        description: | 
          コードの提案の一般提供に向け、安定性、パフォーマンス、可用性、およびユーザーエクスペリエンスの改善に積極的に取り組んでいます。[GitLabでサポートされる機能のマトリックス](https://docs.gitlab.com/ee/policy/experiment-beta-support.html){data-ga-name="gitlab support matrix" data-ga-location="body"}をご確認ください。
        icon: user-laptop
resources:
    data:
      title: AIによるコードの提案の新機能
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: Google AIによるコードの提案
          link_text: 詳細はこちら
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          href: https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#gitlab-duo-code-suggestions-improvements-powered-by-google-ai/
          data_ga_name: Code Suggestions powered by Google AI
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: >-
            JetbrainsとNeoVimのサポートのご紹介
          link_text: 詳細はこちら
          href: https://about.gitlab.com/blog/2023/07/25/gitlab-jetbrains-neovim-plugins/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Intorducing Jetbrains and NeoVim support
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: Visual Studioのサポート
          link_text: 詳細はこちら
          href: https://about.gitlab.com/blog/2023/06/29/gitlab-visual-studio-extension/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Visual studio support
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: Self-Managedのサポート
          link_text: 詳細はこちら
          href: https://about.gitlab.com/blog/2023/06/15/self-managed-support-for-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Self managed support
          data_ga_location: body