template: feature
title: 'Quellcode-Verwaltung'
description: 'Die Quellcodev-Verwaltung (SCM, Source Code Management) in GitLab hilft deinem Entwicklungsteam, zusammenzuarbeiten und die Produktivität zu maximieren, was zu einer schnelleren Bereitstellung und mehr Transparenz führt.'
components:
  feature-block-hero:
    data:
      title: Quellcode-Verwaltung
      subtitle: GitLab macht die Quellcode-Verwaltung einfach
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: Kostenlose Testversion starten
        url: /free-trial/
      image:
        image_url: "/nuxt-images/devops-graphics/create.png"
        hide_in_mobile: true
        alt: ""
  side-navigation:
    links:
      - title: Übersicht
        href: '#overview'
      - title: Vorteile
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: Die Versionskontrolle in GitLab
          content_summary: >-
            hilft deinem Entwicklungsteam, zusammenzuarbeiten und die Produktivität zu maximieren, damit du schneller bereitstellen kannst und mehr Transparenz erhältst. Mit seinem Git-basierten Repository ermöglicht GitLab klare Code-Reviews, Versionskontrolle für Assets, Feedbackschleifen und leistungsstarke Branching-Muster, damit deine Entwickler(innen) Probleme lösen und Werte liefern können.
          content_image: /nuxt-images/features/source-code-management/overview.jpg
          content_heading: Versionskontrolle für alle
          content_list:
            - Skaliere deinen SDLC für die Einführung von Cloud Native
            - Mit dem Git-basierten Repository können Entwickler(innen) von einer lokalen Kopie aus arbeiten
            - Automatische Überprüfung auf Codequalität und Sicherheit bei jedem Commit
            - Integrierte kontinuierliche Integration und kontinuierliche Bereitstellung
      feature-benefit:
        data:
          feature_heading: Transformiere die Softwareentwicklung
          feature_cards:
            - feature_name: Zusammenarbeiten
              icon:
                name: collaboration
                variant: marketing
                alt: "Symbol: Zusammenarbeit"
              feature_description: >-
                Schnellere Bereitstellung… helfen deinem Entwicklungsteam, zusammenzuarbeiten und die Produktivität zu maximieren, was zu einer schnelleren Bereitstellung und mehr Transparenz führt.
              feature_list:
                - 'Überprüfe, kommentiere und verbessere Code'
                - Ermögliche Wiederverwendung und Innersourcing
                - Dateisperren verhindern Konflikte
                - Die robuste WebIDE beschleunigt die Entwicklung auf jeder Plattform.
            - feature_name: Beschleunigen
              icon:
                name: increase
                variant: marketing
                alt: "Symbol: Vergrößern"
              feature_description: >-
                Immer bereitstellen… Versionskontrolle für Assets, Feedbackschleifen und leistungsstarke Branching-Muster helfen deinen Entwickler(inne)n, Probleme zu lösen und Werte zu liefern.
              feature_list:
                - Mit dem Git-basierten Repository können Entwickler(innen) von einer lokalen Kopie aus arbeiten
                - 'Verzweige den Code, nimm Änderungen vor und füge sie in den Haupt-Branch ein'
                - Die robuste WebIDE beschleunigt die Entwicklung auf jeder Plattform.
            - feature_name: Konform und sicher
              icon:
                name: release
                variant: marketing
                alt: "Symbol: Schutzschild mit Häkchen"
              feature_description: >-
                Track and Trace… ermöglicht es Teams, ihre Arbeit mit einer zentralen Informationsquelle zu verwalten.
              feature_list:
                - >-
                  Überprüfe, verfolge und genehmige Codeänderungen mit leistungsstarken Merge Requests
                - Überprüfe Code bei jedem Commit automatisch auf Qualität und Sicherheit
                - >-
                  Vereinfache Prüfung und Konformität mit granularen Zugriffskontrollen und Berichten
  feature-block-related:
    data:
      - Kontinuierliche Integration
      - Epic- & Ticketübersichten
      - Sicherheits-Dashboards
      - Genehmigungsregeln
      - Verwaltung von Sicherheitslücken
  group-buttons:
    data:
      header:
        text: Entdecke weitere Möglichkeiten, wie GitLab die Quellcodeverwaltung unterstützen kann.
        link:
          text: Weitere Lösungen
          href: /solutions/
      buttons:
        - text: Automatische Bereitstellung
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: Kontinuierliche Integration
          icon_left: continuous-delivery
          href: /solutions/continuous-integration/
        - text: Kontinuierliche Software-Sicherheit
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
  report-cta:
    layout: "dark"
    title: Analystenberichte
    reports:
    - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: Lesen Sie den Bericht
    - description: "GitLab als Marktführer im Gartner® Magic Quadrant 2023™ für DevOps-Plattformen anerkannt"
      url: /gartner-magic-quadrant/
      link_text: Lesen Sie den Bericht
  solutions-resource-cards:
    data:
      title: Ressourcen
      link:
        text: Alle Ressourcen anzeigen
      cards:
        - icon:
            name: webcast
            variant: marketing
            alt: "Symbol: Webcast"
          event_type: Webcast
          header: Zusammenarbeit ohne Grenzen – Schnellere Bereitstellung mit GitLab
          link_text: Mehr lesen
          image: /nuxt-images/features/resources/resources_webcast.png
          href: /webcast/collaboration-without-boundaries/
          data_ga_name: Collaboration without Boundaries
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: "Symbol: Fallstudie"
          event_type: Fallstudie
          header: >-
            GitLab fördert die Open-Science-Ausbildung an der Te Herenga Waka – Victoria University of Wellington
          link_text: Mehr lesen
          href: /customers/victoria_university/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: GitLab advances open science education at Te Herenga Waka
          data_ga_location: body
        - icon:
            name: partners
            variant: marketing
            alt: Partners Icon
          event_type: Partners
          header: Entdecken Sie die Vorteile von GitLab auf AWS
          link_text: Jetzt ansehen
          href: /partners/technology-partners/aws/
          image: /nuxt-images/features/resources/resources_partners.png
          data_ga_name: Discover the benefits of GitLab on AWS
          data_ga_location: body
