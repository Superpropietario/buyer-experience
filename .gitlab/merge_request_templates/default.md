/label ~"dex-approval::2-standard"

<!---
This MR will have `dex-approval::2-standard` automatically applied, but please update it as follows. If deciding between two levels, go with the higher of the two: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/marketing/digital-experience/marketing-site-approval-process/index.html.md

Depending on which label is used, you may tag the following people as a `Reviewer` on this MR: https://about.gitlab.com/handbook/marketing/digital-experience/marketing-site-approval-process/#step-3-tag-the-appropriate-people-for-review
--->

**Build Variables:**
- [ ] Use Contentful Preview API

## Step 1: What is changing in this MR? 

<!---Screenshots/videos heavily encouraged --->

*  

| Production | Review app |
| --- | ----------- |
| https://about.gitlab.com/ | WIP |

## Step 2: Ensure that your changes comply with the following, where applicable:

- [ ] I, the Assignee, have run [Axe tools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd) on any updated pages, and fixed the relevant accessibility issues.
- [ ] These changes work on both Safari, Chrome, and Firefox.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes work with our Google Analytics and SEO tools.
- [ ] These changes have been documented as expected.

## Step 3: Ensure that your changes don't cause regressions on key pages, where applicable:

| Production | Review app |
| --- | ----------- |
| https://about.gitlab.com/ | WIP |
| https://about.gitlab.com/ja-jp/ | WIP |
| https://about.gitlab.com/pricing/ | WIP |
| https://about.gitlab.com/ja-jp/pricing/ | WIP |
| https://about.gitlab.com/sales/ | WIP |
| https://about.gitlab.com/gitlab-duo/ | WIP |
| https://about.gitlab.com/enterprise/ | WIP |
| https://about.gitlab.com/platform/ | WIP |
| https://about.gitlab.com/free-trial/ | WIP |

