import { createClient } from 'contentful';
import { CONTENT_TYPES } from '../common/content-types.ts';

const client = createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
  host: 'cdn.contentful.com',
  retryLimit: 100,
});

export default function () {
  let excludedSlugs = [];

  this.nuxt.hook('generate:before', async () => {
    // Checks all SEO entries. An ogURL value is required for the noIndex to properly work here.
    const response = await client.getEntries({
      content_type: CONTENT_TYPES.SEO,
      select: 'fields.ogUrl',
      'fields.noIndex': true,
    });

    excludedSlugs = response.items
      .filter((item) => item.fields && item.fields.ogUrl)
      .map((item) => {
        const url = new URL(item.fields.ogUrl);
        let path = url.pathname;
        path = path.replace(/^\/|\/$/g, '');
        return `/${path}`;
      });

    // This list contains the hardcoded / always uncrawlable pages listed in the sitemap module config within nuxt.config.js
    const hardCodedExcludedList = this.nuxt.options.sitemap.exclude;

    excludedSlugs = excludedSlugs.concat(hardCodedExcludedList);

    return (this.nuxt.options.sitemap.exclude = excludedSlugs);
  });

  this.nuxt.hook('generate:done', (context) => {
    // Grabs every route that Nuxt generated when running 'yarn generate'
    const allRoutes = Array.from(context.generatedRoutes);

    const exclude = this.nuxt.options.sitemap.exclude;

    const filteredRoutes = allRoutes.filter((route) => {
      return !exclude.some((excludedPattern) =>
        route.includes(excludedPattern),
      );
    });

    this.nuxt.options.sitemap.routes = [...filteredRoutes];
  });
}
