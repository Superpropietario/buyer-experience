import Vue from 'vue';

export const SearchEventMixin = Vue.extend({
  beforeMount() {
    document.addEventListener('navSearch', this.onSearch);
  },
  beforeDestroy() {
    document.removeEventListener('navSearch', this.onSearch);
  },
  methods: {
    onSearch(event) {
      this.$router.push({
        path: '/search',
        query: { searchText: event.detail.searchText },
      });
    },
  },
});
